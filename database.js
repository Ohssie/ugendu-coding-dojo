const mysql = require('mysql');

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "underdog",
  database: "dojo"
});

try {
  connection.connect();
  console.log('Database connection has been created!')

  var sqlCreateQuery = "CREATE TABLE IF NOT EXISTS albums (id INT primary key auto_increment, name VARCHAR(255), artist VARCHAR(255))";
  connection.query(sqlCreateQuery, (error, result) => {
    if(error) throw error;
    console.log("Table created!");
  })
} catch (error) {
  console.log(`Unable to connect to Database. Error ${error}`)
}

module.exports = connection;

