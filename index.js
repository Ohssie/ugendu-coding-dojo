'use strict'
const express = require('express');
const bodyParser = require('body-parser')
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var jwt = require('jsonwebtoken')

const app = express()

const port = 3000

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const albums = require('./src/all-albums.js');
const addAlbum = require('./src/add-album.js');

const GOOGLE_CLIENT_ID = "634932627105-2k1or0ggkg5q1a7iesbbn3lqo2q9np80.apps.googleusercontent.com"
const GOOGLE_CLIENT_SECRET = "YJY8nAdU436aKGO2KCdMZ4eO"

var token = '';

passport.use(new GoogleStrategy({
  clientID: GOOGLE_CLIENT_ID,
  clientSecret: GOOGLE_CLIENT_SECRET,
  callbackURL: "http://localhost:3000/auth/google/callback"
},
function(accessToken, refreshToken, profile, done) {
  console.log(`user profile => ${JSON.stringify(profile)}`)
  token = jwt.sign({user: profile.id}, { algorithm: 'RS256' }, function(err, token) {
    console.log(token);
  });
  // User.findOrCreate({ googleId: profile.id }, function (err, user) {
  //   return done(err, user);
  // });
}
));
console.log(`user JWT token => ${token}`)
app.get('/auth/google',
  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));

app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });


app.get('/albums', async() => {
    await albums.getAll();
})

app.post('/add-album',  async(req, res) => {
  await addAlbum.insertAlbum(req, res)
})

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})