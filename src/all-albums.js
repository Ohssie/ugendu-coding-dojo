const database = require('../database')

module.exports.getAll = async () => {
  try {
    /** I will retrieve all the albums in an alphabetical order */
    const albums = await database.query(`select * from albums`);
    console.log(albums)
    if (albums.length > 0) {
      console.log(albums);
      // return {
      //   body: JSON.stringify({
      //     data: albums
      //   })
      // }
    } else {
      console.log(`there are no albums on the database`); 
    }
  } catch (error) {
    console.log(`retrieving albums error: ${error}`)
  }
}